var mook = {
    sobn: function () {
        let raw = `
        Bệnh viện Sản Nhi
        Bệnh viện đa khoa Quảng Ninh
        Trung tâm Y tế thị xã Đông Triều
        Trung tâm Y tế thị xã Quảng Yên
        Bệnh viện Cẩm Phả
        Trung tâm Y tế huyện Hoành Bồ
        Trung tâm Y tế huyện Vân Đồn`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `
        Bệnh viện Sản Nhi
        Bệnh viện đa khoa Quảng Ninh
        Trung tâm Y tế thị xã Đông Triều
        Trung tâm Y tế thị xã Quảng Yên
        Bệnh viện Cẩm Phả
        Trung tâm Y tế huyện Hoành Bồ
        Trung tâm Y tế huyện Vân Đồn`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `TP Hạ Long‎
                Ba Chẽ‎
                Bình Liêu‎
                TP Cẩm Phả‎
                Cô Tô‎
                Đầm Hà‎
                Đông Triều‎ 
                Hải Hà‎
                Hoành Bồ‎ 
                TP Móng Cái‎
                Quảng Yên‎
                Tiên Yên‎
                TP Uông Bí‎ 
                Vân Đồn‎`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `
        Bệnh viện Sản Nhi
        Bệnh viện đa khoa Quảng Ninh
        Trung tâm Y tế thị xã Đông Triều
        Trung tâm Y tế thị xã Quảng Yên
        Bệnh viện Cẩm Phả
        Trung tâm Y tế huyện Hoành Bồ
        Trung tâm Y tế huyện Vân Đồn`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Bệnh viện Sản Nhi
        Bệnh viện đa khoa Quảng Ninh
        Trung tâm Y tế thị xã Đông Triều
        Trung tâm Y tế thị xã Quảng Yên
        Bệnh viện Cẩm Phả
        Trung tâm Y tế huyện Hoành Bồ
        Trung tâm Y tế huyện Vân Đồn`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}
